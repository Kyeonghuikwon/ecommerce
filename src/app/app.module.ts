import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing/app-routing.module';
import {HomeComponentModule} from './pages/home/home.component.module';
import {InMemoryDataService} from './services/in-memory-data.service';
import {HttpClientModule} from '@angular/common/http';
import {HttpClientInMemoryWebApiModule} from 'angular-in-memory-web-api';
import {DataService} from './services/data.service';
import {ShoppingCartService} from './services/shopping-cart.service';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HomeComponentModule,
    HttpClientInMemoryWebApiModule.forRoot(
      InMemoryDataService
    ),
    HttpClientModule
  ],
  providers: [ShoppingCartService, DataService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
