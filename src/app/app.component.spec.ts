import {TestBed, async} from '@angular/core/testing';
import {AppComponent} from './app.component';
import {HomeComponent} from './pages/home/home.component';
import {ProductComponent} from './pages/product/product.component';
import {CartComponent} from './pages/cart/cart.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {HomeComponentModule} from './pages/home/home.component.module';
import {APP_BASE_HREF} from '@angular/common';
import {RouterTestingModule} from '@angular/router/testing';
import {Routes} from '@angular/router';

const appRoutes: Routes = [
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  {path: 'product/:id', component: ProductComponent},
  {path: 'shoppingCart', component: CartComponent}

];

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
      providers: [
        { provide: APP_BASE_HREF, useValue : '/' }
      ],
      imports: [HomeComponentModule, HttpClientTestingModule, RouterTestingModule.withRoutes(appRoutes)],
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
  it(`should have as title 'estore'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('estore');
  }));
});
