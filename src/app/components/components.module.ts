import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HeaderComponent} from './header/header.component';
import {BannerHeaderComponent} from './banner-header/banner-header.component';
import {TableComponent} from './table/table.component';
import {CartOverViewComponent} from './cart-over-view/cart-over-view.component';

@NgModule({
  declarations: [
    HeaderComponent,
    BannerHeaderComponent,
    TableComponent,
    CartOverViewComponent,
  ],
  imports: [
    BrowserModule,
  ],
  exports: [HeaderComponent, BannerHeaderComponent, TableComponent, CartOverViewComponent]
})
export class ComponentsModule {
}
