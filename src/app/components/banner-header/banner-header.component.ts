import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-banner-header',
  templateUrl: './banner-header.component.html',
  styleUrls: ['./banner-header.component.scss']
})
export class BannerHeaderComponent {

  @Input()
  title: string;
  @Input()
  subTitle: string;
  @Input()
  imgSrc: string;

  constructor() {
  }
}
