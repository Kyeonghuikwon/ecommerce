import {Component, Input} from '@angular/core';
import {Router} from '@angular/router';
import {CartItem} from '../../common/models/CartItem';
import {DataService} from '../../services/data.service';
import {Product} from '../../common/models/product';
import {ShoppingCartService} from '../../services/shopping-cart.service';

@Component({
  selector: 'app-cart-over-view',
  templateUrl: './cart-over-view.component.html',
  styleUrls: ['./cart-over-view.component.scss']
})
export class CartOverViewComponent {

  @Input() cartTitle: Array<string> = [];
  @Input() cartItems: CartItem[] = [];

  constructor(public router: Router,
              public shoppingCartService: ShoppingCartService) {
  }

  viewCart() {
    this.router.navigate(['/shoppingCart']);
  }

  getTotalAmount(): number {
    return this.shoppingCartService.getTotalAmount(this.cartItems);
  }

  delete(item: Product) {
    this.shoppingCartService.deleteItem(item.id);
  }
}
