import {Component, EventEmitter, Output} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {

  @Output() clickedOpenCart: EventEmitter<any> = new EventEmitter();

  constructor(public router: Router) {
  }

  goToHome() {
    this.router.navigate(['/home']);
  }

  openCart() {
    this.clickedOpenCart.emit();
  }
}
