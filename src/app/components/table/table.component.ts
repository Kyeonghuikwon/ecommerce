import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {CartItem} from '../../common/models/CartItem';
import {Product} from '../../common/models/product';

/**
 * Table component to display cart items
 * few actions are available check out below
 * 1. increase quantity
 * 2. decrease quantity
 * 3. delete item
 */
@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {

  @Input() columnsTitle: Array<string> = [];
  @Input() cartItems: CartItem[] = [];
  @Input() isQuantityEditable: boolean;
  @Output() clickedInc: EventEmitter<any> = new EventEmitter();
  @Output() clickedDec: EventEmitter<any> = new EventEmitter();
  @Output() clickedDel: EventEmitter<any> = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
  }

  public increment(cartItem: CartItem) {
    this.clickedInc.emit(cartItem);
  }

  public decrement(cartItem: CartItem) {
    this.clickedDec.emit(cartItem);
  }

  public delete(item: Product) {
    this.clickedDel.emit(item);
  }
}
