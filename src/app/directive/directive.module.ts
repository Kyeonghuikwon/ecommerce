import {NgModule} from '@angular/core';
import {ClickOutSideDirective} from './click-out-side.directive';

@NgModule({
  declarations: [
    ClickOutSideDirective
  ],
  imports: [],
  exports: [ClickOutSideDirective]
})
export class DirectiveModule {
}
