import {Directive, EventEmitter, ElementRef, HostListener, Output} from '@angular/core';

@Directive({
  selector: '[appClickOutSide]'
})
export class ClickOutSideDirective {
  constructor(private _elementRef: ElementRef) {
  }

  @Output()
  public clickOutside = new EventEmitter();

  @HostListener('document:click', ['$event.target'])
  public onClick(targetElement) {
    const clickedInside = this._elementRef.nativeElement.contains(targetElement);
    if (targetElement.className === 'cart') {
      return;
    }
    if (!clickedInside) {
      this.clickOutside.emit(null);
    }
  }
}
