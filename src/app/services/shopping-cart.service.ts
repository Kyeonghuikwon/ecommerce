import {Injectable} from '@angular/core';
import {DataService} from './data.service';
import {Product} from '../common/models/product';
import {CartItem} from '../common/models/CartItem';
import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {tap} from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable()
export class ShoppingCartService {
  public cartItems: CartItem[];
  public cartUrl = 'api/cart';

  public constructor(private http: HttpClient, private dataService: DataService) {
    this.dataService.currentCarts.subscribe(cart => this.cartItems = cart);
  }

  public getCartItems(): Observable<CartItem[]> {
    return this.http.get<CartItem[]>(this.cartUrl, httpOptions);
  }

  public addCartItems(cartItem: CartItem): Observable<CartItem> {
    return this.http.post<CartItem>(this.cartUrl, cartItem, httpOptions).pipe(
      tap((_cartItem: CartItem) => console.log(`added cartItem w/ brand=${_cartItem.product.brand}`))
    );
  }

  public async addItem(product: Product, quantity: number) {
    // Create cartItem using product and quantity
    const item = new CartItem();
    item.product = product;
    item.quantity += quantity;

    // Add or Update shopping cart
    const foundIdx = this.cartItems.findIndex(p => p.product.id === product.id);
    if (foundIdx !== -1) {
      this.cartItems.splice(foundIdx, 1, item);
    } else {
      this.cartItems.push(item);
    }

    // Save shoppingCart
    this.dataService.changeCarts(this.cartItems);
    this.addCartItems(item);
  }

  public deleteItem(id) {
    const foundIdx = this.cartItems.findIndex(p => p.product.id === id);
    this.cartItems.splice(foundIdx, 1);
    this.dataService.changeCarts(this.cartItems);
  }

  updateItem(cartItem: CartItem) {
    const foundIdx = this.cartItems.findIndex(p => p.product.id === cartItem.product.id);
    this.cartItems.splice(foundIdx, 1, cartItem);
    this.dataService.changeCarts(this.cartItems);
  }

  /**
   * Calculate totalAmount of items in shoppingCart
   * @param cart ShoppingCart
   */
  public getTotalAmount(cartItems: CartItem[]): number {
    if (!cartItems || cartItems.length < 0) {
      return 0;
    }
    return cartItems
      .map((item: CartItem) => item.quantity * item.product.price)
      .reduce((previous, current) => previous + current, 0);
  }
}
