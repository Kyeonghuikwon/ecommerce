import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {Product} from '../common/models/product';
import {ShoppingCart} from '../common/models/shoppingCart';
import {CartItem} from '../common/models/CartItem';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private products = new BehaviorSubject<Product[]>(null);
  public currentProducts = this.products.asObservable();
  private carts = new BehaviorSubject<CartItem[]>([]);
  public currentCarts = this.carts.asObservable();

  constructor() {
  }

  public changeProducts(products: Product[]) {
    this.products.next(products);
  }

  public changeCarts(cartItem: CartItem[]) {
    this.carts.next(cartItem);
  }
}
