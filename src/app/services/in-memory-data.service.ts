import {InMemoryDbService} from 'angular-in-memory-web-api';
import * as MOCK_PRODUCTS from '../common/products.json';
import {ShoppingCart} from '../common/models/shoppingCart';
import {CartItem} from '../common/models/CartItem';

export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const products = MOCK_PRODUCTS.default;
    const cart = [];
    return {products, cart};
  }
}
