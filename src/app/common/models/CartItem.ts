import {Product} from './product';

export class CartItem {
  product: Product;
  quantity = 0;
}
