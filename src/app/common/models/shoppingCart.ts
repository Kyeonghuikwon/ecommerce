import {CartItem} from './CartItem';

export class ShoppingCart {
  public items: CartItem[] = new Array<CartItem>();
  public totalAmount = 0;
}
