export interface Product {
  id: number;
  title: string;
  brand: string;
  price: number;
  description: string;
  image: string;
}
