import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {CategoryComponent} from './category.component';
import {RouterModule} from '@angular/router';

@NgModule({
  declarations: [
    CategoryComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule
  ],
  exports: [CategoryComponent]
})
export class CategoryComponentModule {
}
