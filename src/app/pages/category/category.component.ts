import {Component, OnInit} from '@angular/core';
import {ProductService} from '../../services/product.service';
import {Router} from '@angular/router';
import {Product} from '../../common/models/product';
import {DataService} from '../../services/data.service';
import {ShoppingCartService} from '../../services/shopping-cart.service';
import {CartItem} from '../../common/models/CartItem';

/**
 * Categories component. Display list of store categories
 */
@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {
  public categories: Product[] = [];
  public cartItems: CartItem[] = [];

  constructor(public productService: ProductService,
              public dataService: DataService,
              public shoppingCartService: ShoppingCartService,
              public router: Router) {
  }

  ngOnInit() {
    this.getCategories();
  }

  async getCategories() {
    this.categories = await this.productService.getProducts().toPromise();
    this.dataService.changeProducts(this.categories);
    this.dataService.currentProducts.subscribe(_product => this.categories = _product);
  }
  /**
   * On select product navigate to specific item details
   * @param product Product
   */
  onSelect(product: Product) {
    this.router.navigate(['/product', product.title, {id: product.id}]);
  }

  /**
   * add product to shopping cart
   * @param product Product
   */
  addToCart(product: Product) {
    this.shoppingCartService.addItem(product, 1);
    this.router.navigate(['/shoppingCart']);
  }
}
