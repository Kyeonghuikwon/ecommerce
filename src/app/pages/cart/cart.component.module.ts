import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CartComponent} from './cart.component';
import {ComponentsModule} from '../../components/components.module';
import {DirectiveModule} from '../../directive/directive.module';

@NgModule({
  declarations: [
    CartComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule,
    ComponentsModule,
    DirectiveModule,
  ],
  exports: [CartComponent]
})
export class CartComponentModule {
}
