import {Component, OnInit} from '@angular/core';
import {DataService} from '../../services/data.service';
import {Product} from '../../common/models/product';
import {ShoppingCartService} from '../../services/shopping-cart.service';
import {CartItem} from '../../common/models/CartItem';
import {Router} from '@angular/router';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {

  public columnsTitle = ['PRODUCT', 'QUANTITY', 'TOTAL', 'ACTION'];
  public cartItems: CartItem[] = [];
  public active = false;


  constructor(public dataService: DataService,
              public shoppingCartService: ShoppingCartService,
              public router: Router) {
  }

  async ngOnInit() {
    this.dataService.currentCarts.subscribe(_cartItems => this.cartItems = _cartItems);
  }

  delete(item: Product) {
    this.shoppingCartService.deleteItem(item.id);
  }

  getTotalAmount(cartItems: CartItem[]): number {
    return this.shoppingCartService.getTotalAmount(cartItems);
  }

  public increment(cartItem: CartItem) {
    cartItem.quantity += 1;
    this.shoppingCartService.updateItem(cartItem);
  }

  public decrement(cartItem: CartItem) {
    if (cartItem.quantity > 0) {
      cartItem.quantity -= 1;
      this.shoppingCartService.updateItem(cartItem);
    }
  }

  continue() {
    this.router.navigate(['/home']);
  }
}
