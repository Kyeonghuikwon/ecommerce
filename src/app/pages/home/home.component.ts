import {Component, OnInit} from '@angular/core';
import {CartItem} from '../../common/models/CartItem';
import {ProductService} from '../../services/product.service';
import {DataService} from '../../services/data.service';
import {ShoppingCartService} from '../../services/shopping-cart.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public banner = {
    title: 'Plates',
    subTitle: 'Sicily Dinnerware Stoneware • Microwavable Dishwasher safe.\n' +
      '      We stand for top quality dinnerware.\n' +
      '      This gorgeous boxed-set of plates and Saucers features some of the most notable Royal Albert patterns from 1950 to 1990.',
    imgSrc: 'assets/images/plates-header.jpg',
  };

  public columnsTitle = ['PRODUCT', 'QUANTITY', 'TOTAL', 'ACTION'];
  public cartItems: CartItem[] = [];
  public active = false;

  constructor(public dataService: DataService) {
  }

  ngOnInit() {
    this.getShoppingCarts();
  }

  async getShoppingCarts() {
    this.dataService.currentCarts.subscribe(cart => this.cartItems = cart);
  }
}
