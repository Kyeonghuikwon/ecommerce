import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HomeComponent} from './home.component';
import {ComponentsModule} from '../../components/components.module';
import {CategoryComponentModule} from '../category/category.component.module';
import {ProductComponentModule} from '../product/product.component.module';
import {CartComponentModule} from '../cart/cart.component.module';
import {DirectiveModule} from '../../directive/directive.module';

@NgModule({
  declarations: [
    HomeComponent,
  ],
  imports: [
    BrowserModule,
    CategoryComponentModule,
    ProductComponentModule,
    CartComponentModule,
    ComponentsModule,
    DirectiveModule,
  ],
  exports: [HomeComponent]
})
export class HomeComponentModule {
}
