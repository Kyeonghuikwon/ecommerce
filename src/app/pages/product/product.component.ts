import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Product} from '../../common/models/product';
import {ProductService} from '../../services/product.service';
import {DataService} from '../../services/data.service';
import {ShoppingCart} from '../../common/models/shoppingCart';
import {ShoppingCartService} from '../../services/shopping-cart.service';
import {CartItem} from '../../common/models/CartItem';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {

  public product: Product;
  public counter = 0;
  private cartItems: CartItem[];

  constructor(public route: ActivatedRoute,
              public router: Router,
              public productService: ProductService,
              public dataService: DataService,
              public shoppingCartService: ShoppingCartService) {
  }

  ngOnInit() {
    this.dataService.currentCarts.subscribe(_carts => this.cartItems = _carts);
    this.route.params.subscribe(async params => {
      if (params && params.id) {
        this.product = await this.productService.getProductById(params.id).toPromise();
      }
    });
  }

  public addCart(product: Product, count: number) {
    this.shoppingCartService.addItem(product, count);
    this.router.navigate(['/shoppingCart']);
  }

  public increment() {
    this.counter += 1;
  }

  public decrement() {
    this.counter -= 1;
  }
}
