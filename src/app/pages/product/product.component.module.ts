import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {ProductComponent} from './product.component';
import {ComponentsModule} from '../../components/components.module';
import {ShoppingCartService} from '../../services/shopping-cart.service';

@NgModule({
  declarations: [
    ProductComponent,
  ],
  imports: [
    BrowserModule,
    ComponentsModule
  ],
  providers: [ShoppingCartService],
  exports: [ProductComponent]
})
export class ProductComponentModule {
}
